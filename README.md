# Java + Cucumber + Selenium

Projeto de automação desenvolvido com o Java + Cucumber + Selenium e integrado com relatório HTML para acompanhamento de execuções que teve como objetivo validar de forma prática sua aplicação em um processo de testes automatizados E2E.

### Requisitos
Para utilização é necessário possuir em sua estação de trabalho:

* IDE para desenvolvimento Java
* JDK >= v1.8.0_281
* JRE >= v1.8.0_281
* Google Chrome
* Maven >= v3.6.3 (Opcional)

### Configurando o ambiente (Windows)

* [Eclipse](https://www.eclipse.org/downloads/packages/release/2019-06/r)
* [Java](https://mauriciogeneroso.medium.com/s%C3%A9rie-configurando-java-overview-79ded2f8d41b)
* [Maven](https://dicasdejava.com.br/como-instalar-o-maven-no-windows)

### Executando os testes do projeto
Após efetuar a configuração do ambiente e realizar o donwload/clone do repositório em sua estação de trabalho, você poderá executar os teste do projeto de duas maneiras:

**IDE de Desenvolvimento:**

Em uma IDE de sua escolha, importe o projeto na workspace da ferramenta.

Na sequência, encontre o arquivo listado abaixo no explorador de pacotes.
```
./src/test/java/br/com/framework/web/runner/ExemploRunnerTest.java
```

Abra o arquivo e realize uma execução utilizando o recurso 'JUnit Test'

**Maven:**

Com o Maven devidamente configurado em sua estação de trabalho, abra um terminal e navegue até a pasta raiz do projeto (pasta que contém o arquivo pom.xml).

Após posicionar o temrinal na pasta mencionada anteriormente, execute o seguinte comando listado abaixo.
```
mvn clean test
```

***Observação:*** _A branch 'master' está configurada para execução headless. Para executar o projeto local e com visualização do navegador utilize a branch 'headed'._

### Relatório de execução de testes
Após efetuar a execução dos testes é possível verificar o relatório detalhado acessando o seguinte arquivo.
```
./target/htmlReporter.html
```

***Observação:*** _Um novo arquivo será gerado a cada execução._

### Execução em CI/CD

**Configuração:**

O script utilizado para desenvolvimento do pipeline de integração contínua pode ser verificado no seguinte arquivo.
```
./.gitlab-ci.yml
```

***Observação:*** _A execução dos testes ocorrerá automaticamente a cada atualização da branch 'master'. Além disso, duas execuções independentes de ações no repositório estão agendadas para ocorrer durante o dia (08h00 e 20h00)._

**Artefatos:**

Os artefatos/evidências gerados a cada execução podem ser virificados ao acessar os detalhes de processamento do job de uma pipeline (ex.: [job details](https://gitlab.com/notfounnd/java-automation-test-framework-web/-/jobs/1076881244) / [artifacts](https://gitlab.com/notfounnd/java-automation-test-framework-web/-/jobs/1076881244/artifacts/browse) / [report](https://notfounnd.gitlab.io/-/java-automation-test-framework-web/-/jobs/1076881244/artifacts/public/htmlReporter.html)).

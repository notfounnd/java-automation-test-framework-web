package br.com.framework.web.exemplo.test;

import java.io.IOException;

import br.com.framework.web.datahelper.FuncionarioDTO;
import br.com.framework.web.exemplo.pages.ExemploAreaLogadaPage;
import br.com.framework.web.exemplo.pages.ExemploCadastroPage;
import br.com.framework.web.exemplo.pages.ExemploLoginPage;
import io.cucumber.java.pt.*;

public class ExemploCadastroSteps {
	
	FuncionarioDTO funcionarioDTO = new FuncionarioDTO();
	
	@Dado ("que tenha efetuado login")
	public void queTenhaEfetuadoLogin() throws IOException {
		ExemploLoginPage exemploLoginPage = new ExemploLoginPage();
		exemploLoginPage.validarElementosExemploPageLogin();
		exemploLoginPage.preencherCampoUsername("test_user");
		exemploLoginPage.preencherCampoPassword("test123!");
		exemploLoginPage.acionarBotaoEntre();
		
		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();
		exemploAreaLogadaPage.verificarLoginComSucesso();
	}
	
	@Quando ("cadastro um novo funcinário")
	public void cadastroUmNovoFuncinario() throws Exception {
		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();
		exemploAreaLogadaPage.acessarFuncionalidadeCadastrarUsuario();
		
		funcionarioDTO.initializeFuncionarioDTO();
		ExemploCadastroPage exemploCadastroPage = new ExemploCadastroPage();
		exemploCadastroPage.verificarFormularioDeCadastro();
		exemploCadastroPage.cadastrarFuncionario(funcionarioDTO);
	}
	
	@Então ("verifico mensagem que a operação foi realizada com sucesso")
	public void verificoMensagemOperaçãoFoiRealizadaComSucesso() throws IOException {
		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();
		exemploAreaLogadaPage.verificarCadastroRealizadoSucesso();
	}
	
	@Então ("o novo funciário é apresentado na listagem")
	public void novoFunciarioApresentadoNaListagem() throws IOException {
		ExemploAreaLogadaPage exemploAreaLogadaPage = new ExemploAreaLogadaPage();
		exemploAreaLogadaPage.verificarFuncinarioNaListagem(funcionarioDTO);
	}
	
}

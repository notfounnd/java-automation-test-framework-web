package br.com.framework.web.exemplo.pages;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import br.com.framework.web.configuration.ExemploTestRules;
import br.com.framework.web.datahelper.FuncionarioDTO;
import br.com.framework.web.utils.ExemploDriverUtils;
import br.com.framework.web.utils.ExemploUtils;

public class ExemploAreaLogadaPage extends ExemploAreaLogadaMap {
	
	/**
	 * Contrutor da classe
	 */
	public ExemploAreaLogadaPage() {
		PageFactory.initElements(ExemploTestRules.getDriver(), this);
	}
	
	// Ações da Página
	
	public void verificarLoginComSucesso() throws IOException {
	
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(menuLinkLogoSite);
		exemploDriverUtils.waitVisibility(menuLinkFuncionarios);
		exemploDriverUtils.waitVisibility(menuLinkNovoFuncionario);
		exemploDriverUtils.waitVisibility(menuLinkSair);
		exemploDriverUtils.waitVisibility(tableInputSearch);
		exemploDriverUtils.waitVisibility(tableTableEntries);
		ExemploUtils.logPassed("Elementos da página 'Área Logada' exibidos com sucesso.");
		
	}
	
	public void verificarTelaInicialExibidaComSucesso() throws IOException {
		
		String stringMenu;
		
		stringMenu = menuLinkSair.getText();
		
		if (stringMenu.equalsIgnoreCase("sair")) {
			ExemploUtils.logPassed("Página 'Área Logada' acessada com sucesso.");
			ExemploUtils.logPrintScreen("verificarTelaInicialExibidaComSucesso");
		} else {
			ExemploUtils.logFailed("Falha ao validar acesso à página 'Área Logada'.");
			ExemploUtils.logPrintScreen("verificarTelaInicialExibidaComSucesso");
			Assert.fail();
		}
		
	}
	
	public void acessarFuncionalidadeCadastrarUsuario() {
		
		menuLinkNovoFuncionario.click();
		ExemploUtils.logPassed("Menu 'Novo Funcionário' acessado com sucesso.");
		
	}

	public void verificarCadastroRealizadoSucesso() throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(alertSuccess);
		if (alertSuccess.isDisplayed()) {
			ExemploUtils.logPassed("Mensagem exibida com sucesso.");
			ExemploUtils.logPrintScreen("verificarCadastroRealizadoSucesso");
		} else {
			ExemploUtils.logFailed("Falha ao validar mensagem de sucesso.");
			ExemploUtils.logPrintScreen("verificarTelaInicialExibidaComSucesso");
			Assert.fail();
		}
		
	}

	public void verificarFuncinarioNaListagem(FuncionarioDTO funcionarioDTO) throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(tableInputSearch);
		tableInputSearch.sendKeys(funcionarioDTO.getNome());
		
		if (funcionarioDTO.getSexo() == "Indiferente") {
			funcionarioDTO.setSexo("Indefinido");
		}
		
		String funcionario = funcionarioDTO.getNome()
				.concat(" " + funcionarioDTO.getCpf())
				.concat(" " + funcionarioDTO.getSexo())
				.concat(" " + funcionarioDTO.getCargo())
				.concat(" " + funcionarioDTO.getAdmissao());
		
		String registros = tableTableEntries.getText();
		registros.contains(funcionario);
		
		if (registros.contains(funcionario)) {
			ExemploUtils.logPassed("Funcionário exibido com sucesso na listagem.");
			ExemploUtils.logPrintScreen("verificarFuncinarioNaListagem");
		} else {
			ExemploUtils.logFailed("Falha ao validar funcionario na listagem.");
			ExemploUtils.logPrintScreen("verificarFuncinarioNaListagem");
			Assert.fail();
		}

	}
	
}

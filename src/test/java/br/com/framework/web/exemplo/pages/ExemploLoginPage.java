package br.com.framework.web.exemplo.pages;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import br.com.framework.web.configuration.ExemploTestRules;
import br.com.framework.web.utils.ExemploDriverUtils;
import br.com.framework.web.utils.ExemploUtils;

public class ExemploLoginPage extends ExemploLoginMap {
	
	/**
	 * Contrutor da classe
	 */
	public ExemploLoginPage() {
		PageFactory.initElements(ExemploTestRules.getDriver(), this);
	}
	
	// Ações da Página
	
	public void validarElementosExemploPageLogin() throws IOException {
		
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(menuLinkLogoSite);
		exemploDriverUtils.waitVisibility(menuLinkCadastreSe);
		exemploDriverUtils.waitVisibility(menuLinkLogin);
		exemploDriverUtils.waitVisibility(formHeaderLogin);
		exemploDriverUtils.waitVisibility(formInputUsername);
		exemploDriverUtils.waitVisibility(formInputPassword);
		exemploDriverUtils.waitVisibility(formButtonEntre);
		exemploDriverUtils.waitVisibility(formLinkCadastreSe);
		ExemploUtils.logPassed("Elementos da página 'Login' exibidos com sucesso.");
		
	}
	
	public void preencherCampoUsername(String username) {
		
		formInputUsername.sendKeys(username);
		ExemploUtils.logPassed("Campo 'usuário' preenchido com sucesso com o valor " + username + ".");
		
	}
	
	public void preencherCampoPassword(String password) throws IOException {
		
		formInputPassword.sendKeys(password);
		ExemploUtils.logPassed("Campo 'senha' preenchido com sucesso com o valor " + password + ".");
		ExemploUtils.logPrintScreen("preencherCampoPassword");
		
	}
	
	public void acionarBotaoEntre() {
		
		formButtonEntre.click();
		ExemploUtils.logPassed("Botão 'entre' acionado com sucesso.");
		
	}
	
	public void verificarLoginSemSucesso() throws IOException {
		ExemploDriverUtils exemploDriverUtils = new ExemploDriverUtils();
		
		exemploDriverUtils.waitVisibility(alertMessageErro);
		ExemploUtils.logPassed("Login não efetuado.");
		
	}
	
	public void verificarLoginApresentaMensagemDeErro() throws IOException {
		
		String stringMensagemErro;
		
		stringMensagemErro = alertMessageErro.getText();
		
		if (stringMensagemErro.contains("ERRO! Usuário ou Senha inválidos")) {
			ExemploUtils.logPassed("Mensagem de erro exibida com sucesso.");
			ExemploUtils.logPrintScreen("verificarLoginApresentaMensagemDeErro");
		} else {
			ExemploUtils.logFailed("Falha ao validar mensamge de erro.");
			ExemploUtils.logPrintScreen("verificarLoginApresentaMensagemDeErro");
			Assert.fail();
		}
		
	}
	
}

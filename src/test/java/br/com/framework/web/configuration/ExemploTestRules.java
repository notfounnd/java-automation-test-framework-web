package br.com.framework.web.configuration;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ExemploTestRules {
	
	private static WebDriver driver;
	private static ExtentHtmlReporter extentHtmlReporter;
	private static ExtentReports extentReports;
	private static ExtentTest extentTest;
	
	/**
	 * Get WebDriver
	 */
	public static WebDriver getDriver() {
		return driver;
	}
	
	/**
	 * Get ExtentTest
	 */
	public static ExtentTest getExtentTest() {
		return extentTest;
	}
	
	@Before // Set Up
	public void beforeScenario(Scenario scenario) {
		
		WebDriverManager.chromedriver().setup();
		
		try {
			if(extentReports == null) {
				extentReports = new ExtentReports();
				extentHtmlReporter = new ExtentHtmlReporter("target/htmlReporter.html");
				extentReports.attachReporter(extentHtmlReporter);
			}
			extentTest = extentReports.createTest(scenario.getName());
			
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--window-size=1920,1080");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--headless");
			
			driver = new ChromeDriver(options);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.navigate().to("http://www.myrobo.tk");
		} catch (Exception e) {
			Assert.fail();
		}
		
	}
	
	@After // Tear Down
	public void afterScenario(Scenario scenario) {
		
		try {
			
			if (scenario.isFailed()) {
				extentTest.log(Status.FAIL, "Cenário \"" + scenario.getName() + "\" executado falhas.");
				extentReports.flush();
			} else {
				extentTest.log(Status.PASS, "Cenário \"" + scenario.getName() + "\" executado com sucesso.");
				extentReports.flush();
			}
			
			if (driver != null) {
				driver.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}
	
}

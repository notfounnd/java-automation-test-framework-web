package br.com.framework.web.utils;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import br.com.framework.web.configuration.ExemploTestRules;

public class ExemploDriverUtils {
	
	private WebDriverWait wait;
	
	/**
	 * Metodo para verificar se o elemento foi carregado corretamente na página
	 * 
	 * @param element - Objeto WebElement do elemento da página
	 * @throws IOException
	 */
	public void waitVisibility(WebElement element) throws IOException {
		try {
			wait = new WebDriverWait(ExemploTestRules.getDriver(), 30);
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (TimeoutException e) {
			Assert.fail();
		}
	}
	
}
